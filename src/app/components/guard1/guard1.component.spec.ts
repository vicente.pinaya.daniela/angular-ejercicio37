import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Guard1Component } from './guard1.component';

describe('Guard1Component', () => {
  let component: Guard1Component;
  let fixture: ComponentFixture<Guard1Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Guard1Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Guard1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
