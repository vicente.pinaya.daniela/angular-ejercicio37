import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Guard2Component } from './guard2.component';

describe('Guard2Component', () => {
  let component: Guard2Component;
  let fixture: ComponentFixture<Guard2Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Guard2Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Guard2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
