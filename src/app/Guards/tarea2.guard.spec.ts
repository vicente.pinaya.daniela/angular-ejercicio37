import { TestBed } from '@angular/core/testing';

import { Tarea2Guard } from './tarea2.guard';

describe('Tarea2Guard', () => {
  let guard: Tarea2Guard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(Tarea2Guard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
