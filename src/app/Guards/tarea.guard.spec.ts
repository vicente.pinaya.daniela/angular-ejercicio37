import { TestBed } from '@angular/core/testing';

import { TareaGuard } from './tarea.guard';

describe('TareaGuard', () => {
  let guard: TareaGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(TareaGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
