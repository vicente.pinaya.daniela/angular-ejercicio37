import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { Guard1Component } from './components/guard1/guard1.component';
import { Guard2Component } from './components/guard2/guard2.component';
import { InicioComponent } from './components/inicio/inicio.component';
import { ServiciosComponent } from './components/servicios/servicios.component';
import { TareaGuard } from './Guards/tarea.guard';
import { Tarea2Guard } from './Guards/tarea2.guard';


const routes: Routes = [
  {path:'inicio',component: InicioComponent},
  {path:'servicios', component: ServiciosComponent},
  {path:'guard1', component: Guard1Component, canActivate:[TareaGuard]},
  {path:'guard2', component:Guard2Component, canActivate:[Tarea2Guard]}, 
  {path:'**', pathMatch:'full', redirectTo:'inicio'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
